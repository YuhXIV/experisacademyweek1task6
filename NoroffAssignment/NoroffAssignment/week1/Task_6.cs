﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class Task_6
    {
        public static void BMI()
        {
            #region Variable
            string input;
            double bmi;
            double weight = 0;
            double height = 0;
            #endregion

            #region Console prompt
            Console.WriteLine("Enter your weight");
            do
            {
                input = Console.ReadLine();
                if (!Double.TryParse(input, out weight)) Console.WriteLine("Only number allowed");


            } while (!Double.TryParse(input, out weight));

            Console.WriteLine("Enter your height");
            do
            {
                input = Console.ReadLine();
                if (!Double.TryParse(input, out height)) Console.WriteLine("Only number allowed");


            } while (!Double.TryParse(input, out height));
            #endregion

            bmi = CalcBMI(weight, height);

            Console.WriteLine($"Your BMI is {bmi} and you are {BodyStatue(bmi)}");
        }

        /// <summary>
        /// Calculate the BMI and return the calculated value
        /// </summary>
        /// <param name="weight">kg</param>
        /// <param name="heigth">meter</param>
        /// <returns></returns>
        static double CalcBMI(double weight, double heigth)
        {
            return weight/(heigth*heigth);
        }

        /// <summary>
        /// Return a string value/info based of the BMI value
        /// </summary>
        /// <param name="bmi">BMI</param>
        /// <returns></returns>
        static string BodyStatue(double bmi)
        {
            if(bmi < 18.5)
            {
                return "Underweight";
            } else if (bmi>=18.5 && bmi <= 24.9)
            {
                return "Normal weight";
            } else if (bmi > 25 && bmi < 30)
            {
                return "Overweight";
            } else
            {
                return "Obese";
            }
        }
    }
}
